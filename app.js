// app.js
const express = require('express');
// const mongoose = require('mongoose');

const app = express();
const port = 3000;

app.use(express.json());

// const connectDB = async () => {
//     try {
//       const conn = await mongoose.connect('mongodb://127.0.0.1:27017/mydatabase1'
//     //   , { useNewUrlParser: true, useUnifiedTopology: true }
//       );
//       console.error('CONNECT TO DATABASE:', conn);
//     } catch (error) {
//         console.error('COULD NOT CONNECT TO DATABASE:', error.message);
//     }
// }

// connectDB();

// // Define user schema and model
// const userSchema = new mongoose.Schema({
//     name: String,
//     email: String,
//     age: Number
// });

// // Define services schema and model
// const serviceSchema = new mongoose.Schema({
//     name: String,
//     description: String
// });

// const User = mongoose.model('User', userSchema);
// const Service = mongoose.model('Service', serviceSchema);

// // Create a new user
// app.post('/users', async (req, res) => {
//     try {
//         const { name, email, age } = req.body;
//         const newUser = new User({ name, email, age });
//         await newUser.save();
//         res.status(201).json(newUser);
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// });

// // Get all users
// app.get('/users', async (req, res) => {
//     try {
//         const users = await User.find();
//         res.json(users);
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// });


// // Create a new user
// app.post('/services', async (req, res) => {
//     try {
//         const { name, description } = req.body;
//         const newService = new Service({ name, description });
//         await newService.save();
//         res.status(201).json(newService);
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// });

// // Get all services
// app.get('/services', async (req, res) => {
//     try {
//         const services = await Service.find();
//         res.json(services);
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// });

app.get('/', (req, res) => {
  res.send('Home Page ...');
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});